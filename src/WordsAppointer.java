import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

class WordsAppointer implements Runnable {
    private final static Object checkMaxNodeLock = new Object();
    private final static Object addToQueueLock = new Object();
    private static Node maxNode;
    private BlockingQueue<Node> queue;

    WordsAppointer(BlockingQueue<Node> queue) {
        this.queue = queue;
        if (maxNode == null) {
            maxNode = new Node(new StringBuilder(), new ArrayList<>());
        }
    }

    @Override
    public void run() {
        Node head;
        List<Node> children;
        while (!Thread.interrupted()) {
            try {
                head = queue.take();
            } catch (InterruptedException e) {
                break;
            }
            children = getNodeChildren(head);
            if (children.isEmpty()) {
                synchronized (checkMaxNodeLock) {
                    if (head.getLength() > maxNode.getLength()) {
                        maxNode = head;
                        printReport(maxNode);
                    }
                }
            } else {
                synchronized (addToQueueLock) {
                    queue.addAll(children);
                }
            }
        }
    }

    private void printReport(Node maxNode) {
        System.out.print("New max sentence: ");
        System.out.println(maxNode);
        System.out.print("Thread name:  ");
        System.out.println(Thread.currentThread().getName());
    }

    List<Node> getNodeChildren(Node node) {
        List<Node> outputList = new LinkedList<>();
        StringBuilder parentSen = node.getCurrentSentence();
        List<String> parentWords = node.getWordsToAppoint();
        char lastChar = Character.toUpperCase(parentSen.charAt(parentSen.length() - 1));
        StringBuilder childSentence;
        List<String> childWords;
        for (int i = 0; i < parentWords.size(); i++) {
            if (lastChar == parentWords.get(i).charAt(0)) {
                childSentence = new StringBuilder(parentSen);
                childSentence.append(" ").append(parentWords.get(i));
                childWords = new LinkedList<>(parentWords);
                childWords.remove(i);
                Node child = new Node(childSentence, childWords);
                outputList.add(child);
            }
        }
        return outputList;
    }
}

class WordsAppointerTest {
    private static BlockingQueue<Node> testQueue = new LinkedBlockingQueue<>();
    private static WordsAppointer subj = new WordsAppointer(testQueue);

    public static void main(String[] args) {
        List<String> testList = new LinkedList<>();
        testList.add("Астрахань");
        testList.add("Абакан");
        testList.add("Анадырь");
        Node node = new Node(new StringBuilder("Анапа"), testList);
        System.out.println(node);
        List<Node> children = subj.getNodeChildren(node);
        System.out.println("children = ");
        for (Node child : children) {
            System.out.println(child);
        }
    }
}


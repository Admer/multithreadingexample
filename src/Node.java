import java.util.List;

public class Node {
    private StringBuilder currentSentence;
    private List<String> wordsToAppoint;
    private int length;

    Node(StringBuilder currentSentence, List<String> wordsToAppoint) {
        this.currentSentence = currentSentence;
        this.wordsToAppoint = wordsToAppoint;
        this.length = currentSentence.toString().split(" ").length;
    }

    StringBuilder getCurrentSentence() {
        return currentSentence;
    }

    List<String> getWordsToAppoint() {
        return wordsToAppoint;
    }

    int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Node{" +
                "Length=" + length +
                ", Current sentence=" + currentSentence +
                ", Words to appoint=" + wordsToAppoint +
                '}';
    }
}

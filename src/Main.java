import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {

   private static BlockingQueue<Node> queue = new LinkedBlockingQueue<>();

    public static void main(String[] args) throws IOException {
        List<String> inputWords = getInputWords();
        initialQueueAppointment(inputWords);
        ThreadUtils.createThreads(queue);
        ThreadUtils.startThreads();
        new Scanner(System.in).nextLine();
        ThreadUtils.interruptThreads();
        }

   private static void initialQueueAppointment(List<String> inputWords) {
        List<String> tempList;
        for (int i = 0; i < inputWords.size(); i++) {
            tempList = new LinkedList<>(inputWords);
            queue.offer(new Node(new StringBuilder(tempList.remove(i)), tempList));
        }
    }

   private static List<String> getInputWords() throws IOException {
        return Files.readAllLines(Paths.get("citiesList.txt"));
    }
}
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

class ThreadUtils {
    private final static int MAX_THREADS = 10;
    static private List<Thread> threads = new ArrayList<>();

    static void createThreads(BlockingQueue<Node> queue) {
        for (int i = 0; i < MAX_THREADS; i++) {
            Thread thread = new Thread(new WordsAppointer(queue));
            threads.add(thread);
        }
    }

    static void startThreads() {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    static void interruptThreads() {
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }
}
